using System.Collections.Generic;
using DevExpress.DataAccess.Json;
using DevExpress.DataAccess.Web;

namespace ServerSideApp.Services;

public class DataSourceStorage : IDataSourceWizardJsonConnectionStorage
{
    private static readonly CustomJsonSource CustomJsonSource = new("{\"test\": \"test\"}");

    private readonly Dictionary<string, JsonDataConnection> _connections = new()
    {
        { "test1", new JsonDataConnection(CustomJsonSource) { Name = "test1" } },
        { "test2", new JsonDataConnection(CustomJsonSource) { Name = "test2" } }
    };

    public IEnumerable<JsonDataConnection> GetConnections()
    {
        return _connections.Values;
    }

    public bool ContainsConnection(string connectionName)
    {
        return _connections.ContainsKey(connectionName);
    }

    public void SaveConnection(string connectionName, JsonDataConnection connection, bool saveCredentials)
    {
        _connections[connectionName] = connection;
    }

    public bool CanSaveConnection => true;

    public JsonDataConnection GetJsonDataConnection(string name)
    {
        return _connections[name];
    }
}
